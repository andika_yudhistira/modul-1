//Soal 1
var word = "javaScript";
var second = "is";
var third = "awesome";
var fourth = "and";
var fifth = "I";
var sixth = "love";
var seventh = "it!";

console.log(`${word} ${second} ${third} ${fourth} ${fifth} ${sixth} ${seventh}`);
console.log('=====================================');
//Tugas 2
const sentence = "I am going to be React Native Developer";

const firstWord = sentence[0];
const secondWord = sentence[2] + sentence[3];
const thirdWord = sentence[5] + sentence[6] + sentence[7] + sentence[8] + sentence[9];
const fourthWord = sentence[11] + sentence[12];
const fifthWord = sentence[14] + sentence[15];
const sixthWord = sentence[17] + sentence[18] + sentence[19] + sentence[20] + sentence[21];
const seventhWord = sentence[23] + sentence[24] + sentence[25] + sentence[26] + sentence[27] + sentence[28];
const eighthWord = sentence[30] + sentence[31] + sentence[32] + sentence[33] + sentence[34] + sentence[35] + sentence[36] + sentence[37] + sentence[38];

console.log('First Word: ' + firstWord);
console.log('Second Word: ' + secondWord);
console.log('Third Word: ' + thirdWord);
console.log('Fourth Word: ' + fourthWord);
console.log('Fifth Word: ' + fifthWord);
console.log('Sixth Word: ' + sixthWord);
console.log('Seventh Word: ' + seventhWord);
console.log('Eighth Word: ' + eighthWord);
console.log('=====================================');
//Tugas 3
const sentence2 = 'wow JavaScript is so cool';

const firstWord2 = sentence2.substring(0, 3);
const secondWord2 = sentence2.substring(4, 14);
const thirdWord2 = sentence2.substring(15, 17);
const fourthWord2 = sentence2.substring(18, 20);
const fifthWord2 = sentence2.substring(21, 25);

console.log('First Word: ' + firstWord2);
console.log('Second Word: ' + secondWord2);
console.log('Third Word: ' + thirdWord2);
console.log('Fourth Word: ' + fourthWord2);
console.log('Fifth Word: ' + fifthWord2);
console.log('=====================================');
//Tugas 4
const sentence3 = 'wow JavaScript is so cool';

const firstWord3 = sentence3.substr(0, 3);
const secondWord3 = sentence3.substr(4, 10);
const thirdWord3 = sentence3.substr(15, 2);
const fourthWord3 = sentence3.substr(18, 2);
const fifthWord3 = sentence3.substr(21, 4);

console.log(`First word: ${firstWord3}, with length: ${firstWord3.length}`);
console.log(`Second Word: ${secondWord3}, with length: ${secondWord3.length}`);
console.log(`Third word: ${thirdWord3}, with length: ${thirdWord3.length}`);
console.log(`Fourth word: ${fourthWord3}, with length: ${fourthWord3.length}`);
console.log(`Fifth word: ${fifthWord3}, with length: ${fifthWord3.length}`);
console.log('=====================================');
